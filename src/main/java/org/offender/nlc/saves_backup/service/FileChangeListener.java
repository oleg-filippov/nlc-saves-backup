package org.offender.nlc.saves_backup.service;

import java.io.File;
import java.util.Collection;

public interface FileChangeListener {

    Collection<File> getFiles();

    void onChange();
}
