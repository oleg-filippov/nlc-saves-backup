package org.offender.nlc.saves_backup.service;

import static org.apache.commons.io.FileUtils.listFiles;
import static org.offender.nlc.saves_backup.util.FileUtils.writeByteArrayToFile;

import java.io.File;
import java.util.Collection;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.offender.nlc.saves_backup.model.CommonData;
import org.offender.nlc.saves_backup.model.Save;
import org.offender.nlc.saves_backup.model.SaveFileData;
import org.offender.nlc.saves_backup.model.SaveFileName;

public class SaveChangeListener implements FileChangeListener {

    private static final Pattern SAVE_FILENAME_PATTERN = Pattern.compile("^.+(\\d{3,})\\..+$");

    private static final Logger logger = Logger.getLogger(SaveChangeListener.class.getName());

    private Save save;
    private String savesPath;

    public SaveChangeListener(Save save, CommonData commonData) {
        this.save = save;
        this.savesPath = commonData.getSavesPath();
    }

    @Override
    public Collection<File> getFiles() {
        return save.getFiles();
    }

    @Override
    public void onChange() {
        if (save.isInitialized()) {
            copySaveFiles();
        } else {
            updateSaveFilesContent();
        }
    }

    private void copySaveFiles() {
        logger.info(save.getType() + " detected. Copying...");

        SaveFileData mainSaveFileData = save.findMainSaveFileData();
        String formattedNextNumber = getSaveFileNextNumber(mainSaveFileData.getSaveFileName());

        save.getSaveFilesData().forEach(saveFileData -> {
            String targetPath = getTargetPath(saveFileData.getSaveFileName(), formattedNextNumber);
            writeByteArrayToFile(new File(targetPath), saveFileData.getFileContent());
            saveFileData.updateFileContent();
        });
    }

    private String getSaveFileNextNumber(SaveFileName saveFileName) {
        IOFileFilter fileFilter = new WildcardFileFilter(getSaveFileWildcard(saveFileName));
        Collection<File> saveFiles = listFiles(new File(savesPath), fileFilter, null);
        int nextNumber = getNextNumber(saveFiles);
        return addLeadingZeros(nextNumber);
    }

    private String getSaveFileWildcard(SaveFileName saveFileName) {
        return saveFileName.getProfileNamePrefix()
                + saveFileName.getSuffix()
                + "*"
                + saveFileName.getDotExtension();
    }

    private int getNextNumber(Collection<File> files) {
        return files.stream()
                    .map(File::getName)
                    .map(SAVE_FILENAME_PATTERN::matcher)
                    .filter(Matcher::find)
                    .map(matcher -> matcher.group(1))
                    .map(Integer::valueOf)
                    .max(Integer::compareTo)
                    .map(max -> max + 1)
                    .orElse(1);
    }

    private String addLeadingZeros(int number) {
        return String.format("%03d", number);
    }

    private String getTargetPath(SaveFileName saveFileName, String nextNumber) {
        return savesPath + getTargetSaveFilename(saveFileName, nextNumber);
    }

    private String getTargetSaveFilename(SaveFileName saveFileName, String nextNumber) {
        return saveFileName.getProfileNamePrefix()
                + saveFileName.getSuffix()
                + nextNumber
                + saveFileName.getDotExtension();
    }

    private void updateSaveFilesContent() {
        save.getSaveFilesData().forEach(SaveFileData::updateFileContent);
    }
}
