package org.offender.nlc.saves_backup.service;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.TimerTask;
import org.offender.nlc.saves_backup.model.FileChangeInfo;

public class FileChangeTask extends TimerTask {

    private FileChangeListener fileChangeListener;
    private List<FileChangeInfo> fileChangeInfos;

    public FileChangeTask(FileChangeListener fileChangeListener) {
        this.fileChangeListener = fileChangeListener;
        this.fileChangeInfos = createFileChangeInfos(fileChangeListener.getFiles());
    }

    private List<FileChangeInfo> createFileChangeInfos(Collection<File> files) {
        return files.stream()
                    .map(FileChangeInfo::new)
                    .collect(toList());
    }

    @Override
    public void run() {
        if (isAllFilesChanged()) {
            fileChangeInfos.forEach(FileChangeInfo::updateLastModified);
            fileChangeListener.onChange();
        }
    }

    private boolean isAllFilesChanged() {
        return fileChangeInfos.stream().allMatch(FileChangeInfo::isFileChanged);
    }
}
