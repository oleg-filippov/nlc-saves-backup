package org.offender.nlc.saves_backup.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import org.offender.nlc.saves_backup.config.SaveConfig;
import org.offender.nlc.saves_backup.config.SaveFileConfig;
import org.offender.nlc.saves_backup.model.CommonData;
import org.offender.nlc.saves_backup.model.Save;
import org.offender.nlc.saves_backup.model.SaveFileData;
import org.offender.nlc.saves_backup.model.SaveFileName;

public class SaveFactory {

    private String savesPath;
    private String profileName;

    public SaveFactory(CommonData commonData) {
        this.savesPath = commonData.getSavesPath();
        this.profileName = commonData.getProfileName();
    }

    public Save save(SaveConfig saveConfig) {
        return new Save(saveConfig.getSaveType(),
                        saveConfig.getCheckPeriodMs(),
                        createSaveFilesData(saveConfig.getSaveFileConfigs())
        );
    }

    private List<SaveFileData> createSaveFilesData(List<SaveFileConfig> saveFileConfigs) {
        return saveFileConfigs.stream()
                              .map(this::createSaveFileData)
                              .collect(toList());
    }

    private SaveFileData createSaveFileData(SaveFileConfig saveFileConfig) {
        SaveFileName saveFileName = new SaveFileName(profileName, saveFileConfig.getFileName());
        return new SaveFileData(savesPath, saveFileName, saveFileConfig.isMainFile());
    }
}
