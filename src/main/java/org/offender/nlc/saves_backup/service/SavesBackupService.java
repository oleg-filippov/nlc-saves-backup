package org.offender.nlc.saves_backup.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Timer;
import java.util.logging.Logger;
import org.offender.nlc.saves_backup.config.Configuration;
import org.offender.nlc.saves_backup.model.CommonData;
import org.offender.nlc.saves_backup.model.Save;

public class SavesBackupService {

    private static final Logger logger = Logger.getLogger(SavesBackupService.class.getName());

    private CommonData commonData;
    private SaveFactory saveFactory;

    public SavesBackupService(CommonData commonData) {
        this.commonData = commonData;
        this.saveFactory = new SaveFactory(commonData);

        logger.info("Game path: " + commonData.getGamePath());
        logger.info("Profile name: " + commonData.getProfileName());
    }

    public void submitSavesBackupTasks() {
        List<Save> saves = Configuration.getSaveConfigs().stream()
                                        .map(saveFactory::save)
                                        .collect(toList());
        saves.forEach(this::submitSavesBackupTask);
    }

    private void submitSavesBackupTask(Save save) {
        SaveChangeListener saveChangeListener = new SaveChangeListener(save, commonData);
        FileChangeTask task = new FileChangeTask(saveChangeListener);
        long checkPeriodMs = save.getCheckPeriodMs();
        new Timer().schedule(task, checkPeriodMs, checkPeriodMs);
    }
}
