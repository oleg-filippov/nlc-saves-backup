package org.offender.nlc.saves_backup.service;

import static org.offender.nlc.saves_backup.util.FileConstants.SEPARATOR;
import static org.offender.nlc.saves_backup.validation.Validator.validateArgs;
import static org.offender.nlc.saves_backup.validation.Validator.validatePaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.offender.nlc.saves_backup.model.CommonData;

public class Initializer {

    private static final String FS_GAME_LTX = "fsgame.ltx";
    private static final String SAVES = "saves" + SEPARATOR;
    private static final String PROFILE_PATH_FORMAT = "%s" + SEPARATOR + "profiles" + SEPARATOR + "%s" + SEPARATOR;
    private static final Pattern PROFILE_NAME_PATTERN = Pattern.compile("\\$profile_dir\\$.+(false|true).+(false|true).+\\$profiles\\$.+\\s+(\\w+)");

    private String gamePath;

    public Initializer(String[] args) {
        validateArgs(args);
        String gamePath = args[0];
        this.gamePath = normalizeGamePath(gamePath);
    }

    private String normalizeGamePath(String gamePath) {
        if (gamePath.endsWith(SEPARATOR)) {
            gamePath = gamePath.substring(0, gamePath.length() - 1);
        }
        return gamePath;
    }

    public CommonData createCommonData() {
        try {
            String profileName = determineProfileName(gamePath);
            String profilePath = String.format(PROFILE_PATH_FORMAT, gamePath, profileName);
            String savesPath = profilePath + SAVES;

            return new CommonData(gamePath, savesPath, profileName);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private String determineProfileName(String gamePath) throws IOException {
        validatePaths(gamePath, FS_GAME_LTX);
        String profileName = Files.lines(Paths.get(gamePath + SEPARATOR + FS_GAME_LTX))
                                  .map(PROFILE_NAME_PATTERN::matcher)
                                  .filter(Matcher::find)
                                  .map(matcher -> matcher.group(3))
                                  .findFirst()
                                  .orElse(null);

        if (profileName == null) {
            throw new IllegalStateException("Unable to determine profile name");
        }

        return profileName;
    }
}
