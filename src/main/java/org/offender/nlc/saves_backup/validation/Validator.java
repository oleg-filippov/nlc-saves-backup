package org.offender.nlc.saves_backup.validation;

import java.io.File;
import org.offender.nlc.saves_backup.util.FileConstants;

public final class Validator {

    private Validator() {
    }

    public static void validateArgs(String[] args) {
        if (args == null || args.length == 0) {
            throw new ValidationException("Game path required");
        }
    }

    public static void validatePaths(String rootPath, String filePath) {
        File path = new File(rootPath);

        if (!path.exists() || !path.isDirectory()) {
            throw new ValidationException("Invalid root path");
        }

        path = new File(rootPath + FileConstants.SEPARATOR + filePath);

        if (!path.exists() || !path.isFile()) {
            throw new ValidationException(filePath + " is invalid");
        }
    }
}
