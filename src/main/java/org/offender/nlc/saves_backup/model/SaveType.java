package org.offender.nlc.saves_backup.model;

public enum SaveType {

    QUICKSAVE,
    AUTOSAVE;
}
