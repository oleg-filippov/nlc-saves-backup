package org.offender.nlc.saves_backup.model;

public class CommonData {

    private String gamePath;
    private String savesPath;
    private String profileName;

    public CommonData(String gamePath, String savesPath, String profileName) {
        this.gamePath = gamePath;
        this.savesPath = savesPath;
        this.profileName = profileName;
    }

    public String getGamePath() {
        return gamePath;
    }

    public String getSavesPath() {
        return savesPath;
    }

    public String getProfileName() {
        return profileName;
    }
}
