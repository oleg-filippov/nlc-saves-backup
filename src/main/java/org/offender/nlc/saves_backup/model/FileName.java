package org.offender.nlc.saves_backup.model;

import static org.offender.nlc.saves_backup.util.FileConstants.DOT;

public class FileName {

    private String name;
    private String extension;

    public FileName(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }

    public String getDotExtension() {
        return DOT + extension;
    }

    public String getFullName() {
        return name + getDotExtension();
    }
}
