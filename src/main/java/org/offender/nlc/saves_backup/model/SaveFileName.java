package org.offender.nlc.saves_backup.model;

import static org.offender.nlc.saves_backup.util.FileConstants.UNDERSCORE;

public class SaveFileName {

    private String profileName;
    private FileName fileName;

    public SaveFileName(String profileName, FileName fileName) {
        this.profileName = profileName;
        this.fileName = fileName;
    }

    public String getProfileName() {
        return profileName;
    }

    public FileName getFileName() {
        return fileName;
    }

    public String getProfileNamePrefix() {
        return profileName + UNDERSCORE;
    }

    public String getSuffix() {
        return fileName.getName();
    }

    public String getDotExtension() {
        return fileName.getDotExtension();
    }

    public String getFullName() {
        return getProfileNamePrefix() + fileName.getFullName();
    }
}
