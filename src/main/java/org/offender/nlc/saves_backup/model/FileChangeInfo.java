package org.offender.nlc.saves_backup.model;

import java.io.File;

public class FileChangeInfo {

    private final File file;
    private long lastModified;

    public FileChangeInfo(File file) {
        this.file = file;
        this.lastModified = file.lastModified();
    }

    public File getFile() {
        return file;
    }

    public long getLastModified() {
        return lastModified;
    }

    public boolean isFileChanged() {
        return file.lastModified() != lastModified;
    }

    public void updateLastModified() {
        lastModified = file.lastModified();
    }
}
