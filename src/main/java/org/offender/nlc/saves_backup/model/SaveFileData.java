package org.offender.nlc.saves_backup.model;

import static org.offender.nlc.saves_backup.util.FileUtils.readFileToByteArray;

import java.io.File;

public class SaveFileData {

    private File file;
    private byte[] fileContent;
    private SaveFileName saveFileName;
    private boolean mainFile;

    public SaveFileData(String savesPath, SaveFileName saveFileName, boolean mainFile) {
        this.file = new File(savesPath + saveFileName.getFullName());
        this.saveFileName = saveFileName;
        this.mainFile = mainFile;
        if (file.exists()) {
            this.fileContent = readFileToByteArray(file);
        }
    }

    public File getFile() {
        return file;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public SaveFileName getSaveFileName() {
        return saveFileName;
    }

    public boolean isMainFile() {
        return mainFile;
    }

    public boolean isInitialized() {
        return fileContent != null;
    }

    public void updateFileContent() {
        fileContent = readFileToByteArray(file);
    }
}
