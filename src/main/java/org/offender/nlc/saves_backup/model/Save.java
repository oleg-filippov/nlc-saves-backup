package org.offender.nlc.saves_backup.model;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;

public class Save {

    private SaveType type;
    private long checkPeriodMs;
    private List<SaveFileData> saveFilesData;

    public Save(SaveType type, long checkPeriodMs, List<SaveFileData> saveFilesData) {
        this.type = type;
        this.checkPeriodMs = checkPeriodMs;
        this.saveFilesData = saveFilesData;
    }

    public SaveType getType() {
        return type;
    }

    public long getCheckPeriodMs() {
        return checkPeriodMs;
    }

    public List<SaveFileData> getSaveFilesData() {
        return saveFilesData;
    }

    public void setSaveFilesData(List<SaveFileData> saveFilesData) {
        this.saveFilesData = saveFilesData;
    }

    public boolean isInitialized() {
        return saveFilesData.stream().allMatch(SaveFileData::isInitialized);
    }

    public List<File> getFiles() {
        return saveFilesData.stream()
                            .map(SaveFileData::getFile)
                            .collect(toList());
    }

    public SaveFileData findMainSaveFileData() {
        return saveFilesData.stream()
                            .filter(SaveFileData::isMainFile)
                            .findFirst()
                            .orElseThrow(() -> new RuntimeException("Main file is not defined for " + type));
    }
}
