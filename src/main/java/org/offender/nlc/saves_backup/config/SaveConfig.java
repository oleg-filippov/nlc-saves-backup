package org.offender.nlc.saves_backup.config;

import java.util.List;
import org.offender.nlc.saves_backup.model.SaveType;

public class SaveConfig {

    private SaveType saveType;
    private long checkPeriodMs;
    private List<SaveFileConfig> saveFileConfigs;

    public SaveConfig(SaveType saveType, long checkPeriodMs, List<SaveFileConfig> saveFileConfigs) {
        this.saveType = saveType;
        this.checkPeriodMs = checkPeriodMs;
        this.saveFileConfigs = saveFileConfigs;
    }

    public SaveType getSaveType() {
        return saveType;
    }

    public long getCheckPeriodMs() {
        return checkPeriodMs;
    }

    public List<SaveFileConfig> getSaveFileConfigs() {
        return saveFileConfigs;
    }
}
