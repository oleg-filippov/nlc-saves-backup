package org.offender.nlc.saves_backup.config;

import org.offender.nlc.saves_backup.model.FileName;

public class SaveFileConfig {

    private FileName fileName;
    private boolean mainFile;

    public SaveFileConfig(FileName fileName, boolean mainFile) {
        this.fileName = fileName;
        this.mainFile = mainFile;
    }

    public FileName getFileName() {
        return fileName;
    }

    public boolean isMainFile() {
        return mainFile;
    }
}
