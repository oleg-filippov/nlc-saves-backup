package org.offender.nlc.saves_backup.config;

import static java.util.Arrays.asList;

import java.util.List;
import org.offender.nlc.saves_backup.model.FileName;
import org.offender.nlc.saves_backup.model.SaveType;

public class Configuration {

    private static final String QUICKSAVE = "quicksave";
    private static final String AUTOSAVE = "autosave";
    private static final String SAV = "sav";
    private static final String DDS = "dds";

    private static List<SaveConfig> saveConfigs;

    static {
        saveConfigs = asList(
                saveConfig(SaveType.QUICKSAVE,
                           1000L,
                           saveFileConfig(new FileName(QUICKSAVE, SAV), true),
                           saveFileConfig(new FileName(QUICKSAVE, DDS), false)
                ),
                saveConfig(SaveType.AUTOSAVE,
                           10000L,
                           saveFileConfig(new FileName(AUTOSAVE, SAV), true)
                )
        );
    }

    private static SaveConfig saveConfig(SaveType saveType, long checkPeriodMs, SaveFileConfig ... saveFileConfigs) {
        return new SaveConfig(saveType, checkPeriodMs, asList(saveFileConfigs));
    }

    private static SaveFileConfig saveFileConfig(FileName fileName, boolean mainFile) {
        return new SaveFileConfig(fileName, mainFile);
    }

    public static List<SaveConfig> getSaveConfigs() {
        return saveConfigs;
    }
}
