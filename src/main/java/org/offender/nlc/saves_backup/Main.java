package org.offender.nlc.saves_backup;

import java.io.IOException;
import java.util.logging.LogManager;
import org.offender.nlc.saves_backup.model.CommonData;
import org.offender.nlc.saves_backup.service.Initializer;
import org.offender.nlc.saves_backup.service.SavesBackupService;

public class Main {

    public static void main(String[] args) {
        configureLogger();
        CommonData commonData = new Initializer(args).createCommonData();
        new SavesBackupService(commonData).submitSavesBackupTasks();
    }

    private static void configureLogger() {
        try {
            LogManager.getLogManager().readConfiguration(Main.class.getClassLoader().getResourceAsStream("logging.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
