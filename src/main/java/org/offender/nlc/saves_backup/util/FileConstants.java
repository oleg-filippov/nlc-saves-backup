package org.offender.nlc.saves_backup.util;

import java.io.File;

public final class FileConstants {

    public static final String SEPARATOR = File.separator;
    public static final String UNDERSCORE = "_";
    public static final String DOT = ".";

    private FileConstants() {
    }

}
