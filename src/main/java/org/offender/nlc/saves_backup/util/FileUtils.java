package org.offender.nlc.saves_backup.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class FileUtils {

    private static final Logger logger = Logger.getLogger(FileUtils.class.getName());

    public static byte[] readFileToByteArray(File file) {
        try {
            return org.apache.commons.io.FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            logger.severe("Error reading file " + file.getPath());
            throw new RuntimeException(e);
        }
    }

    public static void writeByteArrayToFile(File file, byte[] data) {
        try {
            org.apache.commons.io.FileUtils.writeByteArrayToFile(file, data);
        } catch (IOException e) {
            logger.severe("Error writing file " + file.getPath());
            throw new RuntimeException(e);
        }
    }
}
